package main

import (
	"log"
	"net/http"
)

func main() {
	log.Println("Registering Handlers")
	http.Handle("/", http.FileServer(http.Dir("static")))

	log.Println("Starting server")
	log.Fatalln(http.ListenAndServe(":8080", nil))
}

func hello(w http.ResponseWriter, r *http.Request) {

}
